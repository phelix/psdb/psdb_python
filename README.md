Just install the latest version package from:

```bash
python -m pip install psdb-python --index-url https://git.gsi.de/api/v4/projects/1756/packages/pypi/simple
```

Clone the repro with

```bash
git clone https://git.gsi.de/phelix/psdb/psdb_python.git
```

And then install with ``python -m pip install psdb_python``

```python
import psdb_python
```
or
```python
from psdb_python import PSDB
```
and instantiate a new ``PSDB`` Object with your username and password as
```python
my_psdb_obj = PSDB(USERNAME, PASSWORD)
```
and use the given methods and helpers to fetch data from the PSDB.