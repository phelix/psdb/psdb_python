"""PSDB helper tools"""

import io
import base64
import numpy as np
import json
from .psdb import _parse_date, _parse_enum, _status_enum
from datetime import datetime
from PIL import Image


def getdata(instancevalues, name, attachment=False):
    """
    returns the instancevalue specified by name
    takes instancevalues as given by the PSDB
    returns attachment such as physical units in numeric datatype if requested in list of length 2 as [data,attachment]
    attachment is None if datatype gives no attachment
    """
    # datafield = [
    #     "data_string",
    #     "data_numeric",
    #     "data_numeric",
    #     "data_binary",
    #     "data_binary",
    # ]
    datalist = []
    for d in instancevalues:
        data = attach = None
        if d["name"] == name:
            if d["datatype_id"] == 2:  # String
                data = d["data_string"]
            elif d["datatype_id"] == 3:  # Numeric + physical units
                data, attach = _to_float(d["data_numeric"], None), d["data_string"]
            elif d["datatype_id"] == 1:  # Boolean
                data = d["data_numeric"] == 1
            elif d["datatype_id"] == 4:  # Image
                data = d["data_binary"]
            elif d["datatype_id"] == 5:  # 2D
                data, attach = d["data_binary"], d["data_string"]
            elif d["datatype_id"] == 6:  # Stringarray
                data, attach = d["data_binary"], d["data_string"]
            else:
                print(
                    "unsupported datatype_id {} in value {}".format(
                        d["datatype_id"], d["name"]
                    ),
                )
                return

            if attachment:
                datalist.append([data, attach])
            else:
                datalist.append(data)
    if len(datalist) == 1:
        return datalist[0]
    return datalist


def get_refurbish_data(data, value, datatype):
    """This function returns the refurbished data for each datatype as images,
    for example are not returned as an array by the PSDB, which is the same for a string array and 2D data
    """
    match datatype:
        # boolean
        case 1:
            data = getdata(data, value)
        # string
        case 2:
            data = getdata(data, value)
        # numeric
        case 3:
            data = getdata(data, value, attachment=True)
        # image
        case 4:
            rawData = getdata(data, value)
            binData = b64_to_binary(rawData)
            data = binary_to_image(binData)
        # 2D data
        case 5:
            rawData = getdata(data, value, attachment=True)
            binData = b64_to_binary(rawData[0])
            data = binary_to_2Ddata(binData)
        # string array
        case 6:
            rawData = getdata(data, value, attachment=True)
            binData = b64_to_binary(rawData[0])
            data = binary_to_stringarray(binData)

    ### TODO: This has to be handeled correctly as HDF5 does not know the type "None", which will currently just replace the entry by NaN
    if type(data) == type(None):
        # if isinstance(data, None):
        data = np.zeros(np.shape(data)) * np.nan
        # data = 0
    return data


def b64_to_binary(b64data):
    """
    decodes base64 encoded data
    returns the decoded byte data
    """
    rawData = b64data.encode("utf-8")
    binData = base64.decodebytes(rawData)
    return binData


def binary_to_image(binData):
    """
    takes byte data as returned by PSDB and returns the corresponding PIL Image
    """
    binImage = io.BytesIO(binData[4:])
    image = Image.open(binImage)
    return image


def binary_to_2Ddata(binData, empty=None):
    """
    takes byte data as returned by PSDB and returns list [[xdata],[ydata]]
    """
    data = binData[4:]
    data = data.decode("utf-8")
    data = data.replace("E", "e")
    data = np.transpose(
        np.array(
            [
                np.array([_to_float(b, empty) for b in a.split(",")])
                for a in data[:-1].split("\n")
            ]
        )
    )
    return data


def _to_float(string, empty):
    # mangle some numbers
    if string == "":
        return empty
    return float(string)


def binary_to_stringarray(binData):
    """
    takes byte data as returned by PSDB and returns list [[row1],[row2],[row3]]
    """
    data = binData[4:]
    data = data.decode("utf-8")
    data = [row[1:-1].split('","') for row in data.split("\n")]
    return data


def getb64datalength(binData):
    """
    takes byte data as returned by PSDB and reads the length of the data out of the first four bytes
    """
    datalength = int.from_bytes(binData[:4], byteorder="big")
    return datalength


def parse_types(data, keywords, returntype="list"):
    """
    takes a dictionary or list of dictionaries and extracts the given keywords plus corresponding data
    returns extracted data as the given returntype (list or dictionary)
    returntype dictionary only admits two keywords [keywordA,keywordB] returning the data in the order {keywordA:keywordB}
    """
    if not isinstance(data, list):
        data = [data]
    islist = isinstance(keywords, list)
    try:
        if returntype == "list":
            if not islist:
                keywords = ["id", keywords]

            L = len(data)
            res = [0] * L
            i = 0
            for d in data:
                res[i] = [d[keyword] for keyword in keywords]
                i += 1

        elif returntype == "dictionary":
            if not islist or len(keywords) != 2:
                print(
                    "needs list ['a','b'] of keywords for returntype 'dictionary' with {a:b}"
                )
                return {}
            res = {}
            for d in data:
                if isinstance(keywords[1], list):
                    res[d[keywords[0]]] = [d[keywords[1][0]], d[keywords[1][1]]]
                else:
                    res[d[keywords[0]]] = d[keywords[1]]

        else:
            print(
                "Type '{}' not supported! Choose either 'list' or 'dictionary'.".format(
                    returntype
                )
            )
            return None
    except KeyError as e:
        print("KeyError: Key {} not found!".format(e))
        return None
    return res


def dump_json_file(filename, data):
    """
    takes the response of PSDB dumps it as json format to filename specified.
    """
    with open(filename, "w") as jsonFile:
        json.dump(data, jsonFile, indent=4, sort_keys=True, cls=_json_serialize)


def read_json_file(filename):
    """
    returns json format data from the file as it would be an answer of PSDB.
    """
    with open(filename, "r") as jsonFile:
        data = json.load(jsonFile)
        if isinstance(data, list):
            data = _parse_date(data, "created_at")
            data = _parse_date(data, "updated_at")
            data = _parse_date(data, "time")
            data = _parse_enum(data, "status")
        return data


class _json_serialize(json.JSONEncoder):
    """
    Create a JSON Encoder class extending for serialize
    """

    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        elif isinstance(obj, _status_enum):
            return obj.name
        return super().default(obj)
