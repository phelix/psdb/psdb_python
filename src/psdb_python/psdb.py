"""Some PSDB asking class"""

from datetime import datetime
from enum import Enum
import requests


class PSDB:
    """Checked against PSDB (version 0.20.0.1)"""

    _urlAPIbase = "https://psdb.gsi.de/api/"

    def __init__(self, username, pwd):
        """do the init"""
        self._single_access_token = None
        self._post_login(username, pwd)

    def _post_login(self, username, pwd):
        """do the login"""
        url = self._urlAPIbase + "user_sessions"
        payload = {"user_session": {"login": username, "password": pwd}}
        try:
            r = requests.post(url, json=payload)
            r.raise_for_status()
            if r.status_code == requests.codes.ok:
                print("login successful!")
                sat = r.json()
                self._single_access_token = sat["single_access_token"]
        except requests.exceptions.RequestException as errh:
            print(errh)

    # without auth without params

    def get_datatypes(self):
        """
        return all available measurement data types such as numeric, string, etc.
        """
        type = "datatypes"
        return self._get_types(type)

    def get_shottypes(self):
        """
        return all available shot types such as “experiment shot”, “test shot”, etc.
        """
        type = "shottypes"
        return self._get_types(type)

    def get_subsystems(self):
        """
        return all available subsystems such as “MAS”, ”COS”, etc.
        """
        type = "subsystems"
        return self._get_types(type)

    def get_classtypes(self):
        """
        return all class types such as PH_gentec_Powermeter, etc.
        """
        type = "classtypes"
        return self._get_types(type)

    def get_experimentareas(self):
        """
        return all available experiment areas such as PTA, Z6, etc.
        """
        type = "experimentareas"
        return self._get_types(type)

    def get_measparamtypes(self):
        """
        return all measurement parameter types such as temperature, pressure, etc.
        """
        type = "measparamtypes"
        return self._get_types(type)

    def _get_types(self, type):
        """
        generalization of the get_***types methods. Returns all available data of the specified type.
        """
        available = [
            "datatypes",
            "shottypes",
            "subsystems",
            "classtypes",
            "experimentareas",
            "measparamtypes",
        ]
        if type not in available:
            print("Type '{}' not available! Chose one of {}".format(type, available))
            return None
        else:
            url = self._urlAPIbase + type
            total_response = self._HTTP_Request(url)
            if total_response is not None:
                total_response = _parse_date(total_response, "created_at")
                total_response = _parse_date(total_response, "updated_at")
            return total_response

    # END without auth without params

    # with auth without params or one param

    def get_instance(self, instance_id):
        """
        returns a single system instance (device) with the given database id.
        """
        url = self._urlAPIbase + "instances/" + str(instance_id)
        total_response = self._HTTP_Request(url, sat=self._single_access_token)
        if total_response is not None:
            total_response = _parse_date(total_response, "created_at")
            total_response = _parse_date(total_response, "updated_at")
        return total_response

    def get_shot(self, shot_id):
        """
        Returns a single shot object with the specified shot id
        """
        url = self._urlAPIbase + "shots/" + str(shot_id)
        total_response = self._HTTP_Request(url, sat=self._single_access_token)
        if total_response is not None:
            total_response = _parse_date(total_response, "created_at")
            total_response = _parse_date(total_response, "updated_at")
        return total_response

    def get_experiments(self):
        """
        This returns all experiments available to the current user.
        """
        url = self._urlAPIbase + "experiments/"
        total_response = self._HTTP_Request(url, sat=self._single_access_token)
        if total_response is not None:
            total_response = _parse_date(total_response, "created_at")
            total_response = _parse_date(total_response, "updated_at")
        return total_response

    # END with auth without params or one param

    # with auth with params

    def get_instances(self, **kwargs):
        """
        return system instances (devices) such as PA_Input_Nearfield_Cam, etc. If no parameters are given, all instances are returned. Otherwise the result is filtered by the given params.
        Note: If more than one shot_id is given, an inclusive list of instances involved in the given shots is returned. This means a particular instance in the list might not be available for each shot.

        **kwargs
        ---
        keywords = ["shot_id", "subsystem_id", "classtype_id"]
        """
        keywords = ["shot_id", "subsystem_id", "classtype_id"]
        type = "instances"
        return self._get_param_types(type, keywords, **kwargs)

    def get_shots(self, **kwargs):
        """
        Returns a list of shots filtered by the given filter parameters.

        **kwargs
        ---
        keywords = [ "id", "experiment_id", "shottype_id", "from_shot_id", "to_shot_id", "from_date", "to_date"]
        """
        keywords = [
            "id",
            "experiment_id",
            "shottype_id",
            "from_shot_id",
            "to_shot_id",
            "from_date",
            "to_date",
        ]
        type = "shots"
        return self._get_param_types(type, keywords, **kwargs)

    def get_instancevalues(self, **kwargs):
        """
        Returns a list of actual measurement data optionally filtered by given filter parameters.
        Check the schema definition for interpretation of the data fields.
        Note: Because of the possibly huge amount of data, instancevalues are transmitted in sets of 10 values (= 1 chunk). The chunk parameter denotes the chunk number starting with 1 for the first 10 data values, Chunk parameter value 2 hence returns the second 10 values and so on. If the chunk parameter id less than 1 or greater than the number of available chunks (see next command) an empty array is returned.

        **kwargs
        ---
        keywords = ["shot_id", "instance_id", "experiment_id", "name", "chunk"]
        """
        keywords = ["shot_id", "instance_id", "experiment_id", "name", "chunk"]
        type = "instancevalues"
        if "chunk" not in kwargs:
            i = 1
            single_response = None
            total_response = []
            chunks = self.get_instancevalues_chunk_count(**kwargs)
            if chunks != {}:
                if chunks > 1:
                    while i <= chunks and single_response != []:
                        single_response = self._get_param_types(
                            type, keywords, chunk=i, **kwargs
                        )
                        i += 1
                        total_response += single_response
                else:
                    total_response = self._get_param_types(type, keywords, **kwargs)
        else:
            total_response = self._get_param_types(type, keywords, **kwargs)
        return total_response

    def get_instancevalues_chunk_count(self, **kwargs):
        """
        Returns the number of data chunks to be expected by the the command above. Hence, the same filter parameters should be used.

        **kwargs
        ---
        keywords = ["shot_id", "instance_id", "experiment_id", "name"]
        """
        keywords = ["shot_id", "instance_id", "experiment_id", "name"]
        type = "instancevalues/chunks"
        return self._get_param_types(type, keywords, **kwargs)

    def get_attachments(self, **kwargs):
        """This returns only file attachments available to the current user.
        * Because of the possibly huge amount of data, attachments are transmitted in sets of 10 values (= 1 chunk). The chunk parameter denotes the chunk number starting with 1 for the first 10 attachments, chunk parameter value 2 hence returns the second 10 attachments and so on. If the chunk parameter id less than 1 or greater than the number of available chunks (see next command) an empty array is returned.
        * Attachments are filtered by either experiment_id, shot_id, or logentry_id since an attachment can only belong to one of these groups. Without one of the above parameters, the result is empty. If parameters are mixed, the result might be undefined!
        * The content field is Base64 encoded.

        **kwargs Parameters
        ----------------
        keywords = ["shot_id", "experiment_id", "logentry_id", "chunk"]
        """
        keywords = ["shot_id", "experiment_id", "logentry_id", "chunk"]
        type = "attachments"

        if ("chunk" not in kwargs and len(kwargs) > 1) or (
            "chunk" in kwargs and len(kwargs) > 2
        ):
            print("Warning: too many keyword specifications given!")
            return {}
        else:
            return self._get_param_types(type, keywords, **kwargs)

    def get_attachments_chunk_count(self, **kwargs):
        """return the number of chunks for given filter parameters.
        * Attachments are filtered by either experiment_id, shot_id, or logentry_id since an attachment can only belong to one of these groups. Without one of the above parameters, the result is empty. If parameters are mixed, the result might be undefined!

        **kwargs Parameters
        ----------------
        keywords = ["instance_id", "shot_id", "logentry_id"]
        """
        keywords = ["instance_id", "shot_id", "logentry_id"]
        type = "attachments/chunks"
        if len(kwargs) > 1:
            print("Warning: too many keyword specifications given!")
            return {}
        else:
            return self._get_param_types(type, keywords, **kwargs)

    def get_instancevaluesets_count(self, **kwargs):
        """
        Return the number of measurement value sets that would be downloaded with the given filter parameters. This function can be used for a "progress calculation" while downloading large amount of data.

        **kwargs Parameters
        ----------------
        keywords = ["shot_id", "instance_id", "experiment_id", "shottype_id", "from_date", "to_date", "from_shot_id", "to_shot_id"]
        """
        keywords = [
            "shot_id",
            "instance_id",
            "experiment_id",
            "shottype_id",
            "from_date",
            "to_date",
            "from_shot_id",
            "to_shot_id",
        ]
        type = "instancevaluesets/count"
        return self._get_param_types(type, keywords, **kwargs)

    def get_measparams(self, **kwargs):
        """
        return all existing measurement parameters of a timeseries. The index of a parameter can be used as measparam_id for the timeseries api call for filtering.

        **kwargs Parameters
        ----------------
        keywords = ["measparamtype_id"]
        """
        keywords = ["measparamtype_id"]
        type = "measparams"
        return self._get_param_types(type, keywords, **kwargs)

    def get_timeseries(self, **kwargs):
        """
        return timeseries data, If no parameters are given, all instances are returned. Otherwise the result is filtered by the given params.
        Note: Because of the possibly huge amount of data, instancevalues are transmitted in sets of 1000 values (= 1 chunk). The chunk parameter denotes the chunk number starting with 1 for the first 1000 data values, Chunk parameter value 2 hence returns the second 1000 values and so on. If the chunk parameter id less than 1 or greater than the number of available chunks (see next command) an empty array is returned.

        **kwargs
        ---
        keywords = ["measparam_id", "from_date", "to_date", "chunk"]
        """
        keywords = ["measparam_id", "from_date", "to_date", "chunk"]
        type = "timeseries"
        if "chunk" not in kwargs:
            i = 1
            single_response = None
            total_response = []
            chunks = self.get_timeseries_chunk_count(**kwargs)
            if chunks != {}:
                if chunks > 1:
                    while i <= chunks and single_response != []:
                        single_response = self._get_param_types(
                            type, keywords, chunk=i, **kwargs
                        )
                        i += 1
                        total_response += single_response
                else:
                    total_response = self._get_param_types(type, keywords, **kwargs)
        else:
            total_response = self._get_param_types(type, keywords, **kwargs)
        if total_response is not None:
            total_response = _parse_date(total_response, "time")
            total_response = _parse_numeric(total_response, "data")
            total_response = _parse_numeric(total_response, "lower_limit")
            total_response = _parse_numeric(total_response, "upper_limit")
        return total_response

    def get_timeseries_chunk_count(self, **kwargs):
        """
        Returns the number of data chunks to be expected by the the command above. Hence, the same filter parameters should be used. If this function returns zero, no data hase been found and the above command would return an empty array.

        **kwargs
        ---
        keywords = ["measparam_id", "from_date", "to_date"]
        """
        keywords = ["measparam_id", "from_date", "to_date"]
        type = "timeseries/chunks"
        return self._get_param_types(type, keywords, **kwargs)

    def get_timestatuses(self, **kwargs):
        """
        return status timeseries data, If no parameters are given, all data is returned. Otherwise the result is filtered by the given params.
        Note: Because of the possibly huge amount of data, instancevalues are transmitted in sets of 1000 values (= 1 chunk). The chunk parameter denotes the chunk number starting with 1 for the first 1000 data values, Chunk parameter value 2 hence returns the second 1000 values and so on. If the chunk parameter id less than 1 or greater than the number of available chunks (see next command) an empty array is returned.

        **kwargs
        ---
        keywords = ["instance_id", "from_date", "to_date", "chunk"]
        """
        keywords = ["instance_id", "from_date", "to_date", "chunk"]
        type = "timestatuses"
        if "chunk" not in kwargs:
            i = 1
            single_response = None
            total_response = []
            chunks = self.get_timestatuses_chunk_count(**kwargs)
            if chunks != {}:
                if chunks > 1:
                    while i <= chunks and single_response != []:
                        single_response = self._get_param_types(
                            type, keywords, chunk=i, **kwargs
                        )
                        i += 1
                        total_response += single_response
                else:
                    total_response = self._get_param_types(type, keywords, **kwargs)
        else:
            total_response = self._get_param_types(type, keywords, **kwargs)
        if total_response is not None:
            total_response = _parse_date(total_response, "time")
            total_response = _parse_enum(total_response, "status")
        return total_response

    def get_timestatuses_chunk_count(self, **kwargs):
        """
        Returns the number of data chunks to be expected by the the command above. Hence, the same filter parameters should be used. If this function returns zero, no data hase been found and the above command would return an empty array.

        **kwargs
        ---
        keywords = ["instance_id", "from_date", "to_date"]
        """
        keywords = ["instance_id", "from_date", "to_date"]
        type = "timestatuses/chunks"
        return self._get_param_types(type, keywords, **kwargs)

    def _get_param_types(self, type, keywords, **kwargs):
        """
        Gerneralized funtion for all requests with auth and params.
        """
        available = [
            "instances",
            "instancevalues/chunks",
            "instancevalues",
            "shots",
            "instancevaluesets",
            "instancevaluesets/count",
            "attachments",
            "attachments/chunks",
            "measparams",
            "timeseries",
            "timeseries/chunks",
            "timestatuses",
            "timestatuses/chunks",
        ]
        if type not in available:
            print("Type '{}' not available! Chose one of {}".format(type, available))
            return None
        if self._broken_kwargs(keywords, **kwargs):
            print("keywords not valid. chose one of {}".format(keywords))
            return None
        url = self._urlAPIbase + type
        arguments = self._psdb_array_kwargs(**kwargs)
        total_response = self._HTTP_Request(
            url, sat=self._single_access_token, payload=arguments
        )
        if total_response is not None:
            total_response = _parse_date(total_response, "created_at")
            total_response = _parse_date(total_response, "updated_at")
            total_response = _parse_numeric(total_response, "data_numeric")
        return total_response

    # END with auth with params

    def _HTTP_Request(self, url, sat=None, payload=None):
        """
        posts a url request to the given url and returns the json response as a dictionary
        """
        # payload = {"user_session": {"login": username, "password": pwd}}
        try:
            r = requests.get(url, params=payload, data={"user_credentials": sat})
            # print(r.url)
            r.raise_for_status()
            if r.status_code == requests.codes.ok:
                return r.json()
        except requests.exceptions.RequestException as errh:
            print(errh)
            return None

    def _broken_kwargs(self, keywords, **kwargs):
        """
        checks if keywords given in **kwargs exist in the dictionary in question (given as keywords)
        """
        for key, _ in kwargs.items():
            if key not in keywords:
                return True
        return False

    def _psdb_array_kwargs(self, **kwargs):
        """
        for PSDB API to enable a list as argument, the key needs to end with []
        dont for chunk, from_date, to_date, from_shot_id, to_shot_id
        """
        arguments = {}
        notarrayarguments = [
            "chunk",
            "from_date",
            "to_date",
            "from_shot_id",
            "to_shot_id",
        ]
        for key, _ in kwargs.items():
            if key in notarrayarguments:
                arguments[key] = kwargs[key]
            elif key not in notarrayarguments:
                arguments[key + "[]"] = kwargs[key]
        return arguments


def _parse_date(data, datefield):
    """
    convert datestring in created_at and updated_at to datetime
    """
    if isinstance(data, list):
        for L in range(len(data)):
            if datefield in data[L].keys():
                data[L][datefield] = datetime.fromisoformat(data[L][datefield])
    elif isinstance(data, dict):
        if datefield in data.keys():
            data[datefield] = datetime.fromisoformat(data[datefield])
    return data


def _parse_numeric(data, numericfield):
    """
    convert data_numeric string field to float
    """
    if isinstance(data, list):
        for L in range(len(data)):
            if numericfield in data[L].keys():
                if data[L][numericfield] is not None:
                    data[L][numericfield] = float(data[L][numericfield])
    elif isinstance(data, dict):
        if numericfield in data.keys():
            if data[numericfield] is not None:
                data[numericfield] = float(data[numericfield])
    return data


def _parse_enum(data, enumfield):
    """
    convert status string field to enum. Used in Timestatus data.
    """
    if isinstance(data, list):
        for L in range(len(data)):
            if enumfield in data[L].keys():
                if data[L][enumfield] is not None:
                    data[L][enumfield] = _status_enum[data[L][enumfield]]
    elif isinstance(data, dict):
        if enumfield in data.keys():
            if data[enumfield] is not None:
                data[enumfield] = _status_enum[data[enumfield]]
    return data


class _status_enum(Enum):
    """
    enum class for status string field of Timestatus data.
    As it is in psdb/app/models/timestatus.rb

    """

    not_linked = -1
    no_info = 0
    off = 1
    on = 2
    failure = 3
    alarm = 4
    status_1 = 5
    status_2 = 6
    status_3 = 7
