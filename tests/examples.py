"""PSDB get data examples"""

import matplotlib.pyplot as plt
from re import split

# import os
# import sys

# sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from psdb_python import PSDB, helper


def get_image_example(username, password, shot_id=19739):
    """Get an Image"""
    sensor = "MAS_Nearfield_Cam"
    value = "Image"
    ps = PSDB(username, password)
    inst = ps.get_instances(shot_id=shot_id)
    inst = helper.parse_types(inst, ["name", "id"], returntype="dictionary")
    data = ps.get_instancevalues(shot_id=shot_id, instance_id=inst[sensor])
    # L = psdb_helper.parse_types(data, ["name", "datatype_id"], returntype="list")
    rawData = helper.getdata(data, value)
    binData = helper.b64_to_binary(rawData)
    image = helper.binary_to_image(binData)
    image.show()
    return


def get_2dData_example(username, password, shot_id=19739):
    """Get Scope data"""
    sensor = "phoszi001_Tektronix7000"
    value = "Channel1: Data"
    ps = PSDB(username, password)
    inst = ps.get_instances(shot_id=shot_id)
    inst = helper.parse_types(inst, ["name", "id"], returntype="dictionary")
    data = ps.get_instancevalues(shot_id=shot_id, instance_id=inst[sensor])
    # L = psdb_helper.parse_types(data, ["name", "datatype_id"], returntype="list")
    rawData = helper.getdata(data, value, attachment=True)
    binData = helper.b64_to_binary(rawData[0])
    data = helper.binary_to_2Ddata(binData)
    labels = split("[,\s]+", rawData[1])
    fig, ax = plt.subplots()
    ax.plot(data[0], data[1])
    ax.grid()
    ax.set(
        xlabel="{} ({})".format(labels[0], labels[2]),
        ylabel="{} ({})".format(labels[1], labels[3]),
        title="{} {}".format(sensor, value),
    )
    plt.show()
    return


def get_numeric_example(username, password, shot_id=19739):
    """Get a numeric Value"""
    sensor = "MAS_Nearfield_Cam"
    value = "Gain"
    ps = PSDB(username, password)
    inst = ps.get_instances(shot_id=shot_id)
    inst = helper.parse_types(inst, ["name", "id"], returntype="dictionary")
    data = ps.get_instancevalues(shot_id=shot_id, instance_id=inst[sensor])
    # L = psdb_helper.parse_types(data, ["name", "datatype_id"], returntype="list")
    rawData = helper.getdata(data, value)
    return rawData


def get_string_example(username, password, shot_id=19739):
    """Get a String"""
    sensor = "MAS_OO_Spectrometer"
    value = "Status"
    ps = PSDB(username, password)
    inst = ps.get_instances(shot_id=shot_id)
    inst = helper.parse_types(inst, ["name", "id"], returntype="dictionary")
    data = ps.get_instancevalues(shot_id=shot_id, instance_id=inst[sensor])
    # L = psdb_helper.parse_types(data, ["name", "datatype_id"], returntype="list")
    rawData = helper.getdata(data, value)
    return rawData


def get_boolean_example(username, password, shot_id=19739):
    """Get a Boolean"""
    sensor = "MAS_Nearfield_Cam"
    value = "Trigger Polarity"
    ps = PSDB(username, password)
    inst = ps.get_instances(shot_id=shot_id)
    inst = helper.parse_types(inst, ["name", "id"], returntype="dictionary")
    data = ps.get_instancevalues(shot_id=shot_id, instance_id=inst[sensor])
    # L = psdb_helper.parse_types(data, ["name", "datatype_id"], returntype="list")
    rawData = helper.getdata(data, value)
    return rawData


def get_stringarray_example(username, password, shot_id=20089):
    """Get a String array (Log)"""
    sensor = "PP-Log"
    value = "Logfile"
    ps = PSDB(username, password)
    inst = ps.get_instances(shot_id=shot_id)
    inst = helper.parse_types(inst, ["name", "id"], returntype="dictionary")
    data = ps.get_instancevalues(shot_id=shot_id, instance_id=inst[sensor])
    # L = psdb_helper.parse_types(data, ["name", "datatype_id"], returntype="list")
    rawData = helper.getdata(data, value, attachment=True)
    binData = helper.b64_to_binary(rawData[0])
    data = helper.binary_to_stringarray(binData)
    labels = [row[1:-1].split('","') for row in rawData[1].split("\n")]
    print(labels)
    for a in data:
        print(a)
    return


def get_attachment_example(username, password, shot_id=23636):
    """Get a file attachment from a shot and dump into file"""
    ps = PSDB(username, password)
    data = ps.get_attachments(shot_id=shot_id)
    for i in range(len(data)):
        filename = data[i]["filename"]
        file_content = helper.b64_to_binary(data[i]["content"])
        with open(filename, "wb") as binary_file:
            binary_file.write(file_content)
    return
