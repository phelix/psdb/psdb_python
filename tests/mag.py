from psdb_python import PSDB, dump_json_file, read_json_file

from dotenv import dotenv_values

config = dotenv_values(".env")

USERNAME = config["USERNAME2"]
PASSWORD = config["PASSWORD2"]
data = []
psa = PSDB(USERNAME, PASSWORD)
shot_id = list(range(24895, 24907))
shot_id = 24895
# data = psa.get_shottypes()
# data = psa.get_experiments()
# data = psa.get_experimentareas()
# data = psa.get_datatypes()
# data = psa.get_classtypes()
# data = psa.get_measparamtypes()
# data = psa.get_subsystems()

# data = psa.get_instancevalues_chunk_count(instance_id=99, shot_id=shot_id)
# data = psa.get_instancevalues(instance_id=54, shot_id=shot_id)
# data = psa.get_instances(classtype_id=13, subsystem_id=[8, 46])
# data = psa.get_instance(449)
# data = psa.get_measparams(measparamtype_id=2)
data = psa.get_shots(id=shot_id, from_shot_id=2491)
# data = psa.get_shot(24901)
# data = psa.get_attachments_chunk_count(shot_id=[23628, 23636])
# data = psa.get_attachments(shot_id=[23628])

# geht grad net observer
# data = psa.get_instancevaluesets_count(instance_id=99, shot_id=shot_id)
# data = psa.get_timeseries_chunk_count(measparam_id=4, from_date="2024-06-22")
# data = psa.get_timeseries(measparam_id=11, chunk=1, from_date="2024-08-22")
# data = psa.get_timestatuses_chunk_count(instance_id=99)
# data = psa.get_timestatuses(instance_id=99, chunk=1)

### examples
# import examples

# examples.get_2dData_example(USERNAME, PASSWORD)
# examples.get_image_example(USERNAME, PASSWORD)
# examples.get_attachment_example(USERNAME, PASSWORD)
# data = examples.get_boolean_example(USERNAME, PASSWORD)
# data = examples.get_numeric_example(USERNAME, PASSWORD)
# data = examples.get_string_example(USERNAME, PASSWORD)
# data = examples.get_stringarray_example(USERNAME, PASSWORD)

print(data)

# filename = "tests\\cd.json"
# # # Write new values
# dump_json_file(filename, data)

# data = []

# # Read existing data
# data = helper.read_json_file(filename)

# if isinstance(data, list):
#     print(len(data))
#     print(data[0])
# else:
#     print(data)
